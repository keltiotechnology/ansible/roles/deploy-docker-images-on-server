# Deploy docker images on server

Deploy multiple docker images on server using ansible. This role make sur all the networks and volumes are created then it deploy all the wanted images.

## Requirements

- Ansible (locally)
- Docker (on server)
- Pip package: docker (on server)

## Role Variables

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| docker_images | List of docker images to deploy | `list(DockerImageObject)` | `[]` | yes |

### DockerImageObject

This object inherits all the parameters from https://docs.ansible.com/ansible/2.8/modules/docker_container_module.html

## Outputs

There is no outputs for this role

<b>Note:</b> Outputs are displayed info during last tasks.
